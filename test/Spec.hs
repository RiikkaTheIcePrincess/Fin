{-# LANGUAGE OverloadedStrings #-}
import Test.Hspec
import Test.QuickCheck
import Text.Megaparsec (parse)
import Control.Exception (evaluate)
import qualified Data.Vector as V
import qualified Data.Map as Map
import System.Console.Haskeline (runInputT, defaultSettings)
--Local
import RiForth.Eval
import RiForth.Env
import RiForth.RFVal
import RiForth.Prim
import RiForth.Parser

test0 :: String
test0 = unlines ["2 (",
         "((3 >) (\"Greater than 3\" . cr))",
         "((3 <) (\"Smaller than 3\" . cr))",
         "((true) (\"Exactly 3\" . cr))",
         ") case"]
test1 :: String
test1 = unlines [":space \" \" print;"
  ,":star \"*\" print;"
  ,":top 0 (star) do cr;"
  ,":bottom top;"
  ,":middle star 2 - 0 (space) do star cr;"
  ,":box cr over top 2 - 0 (dup middle) do bottom;"
  ,"80 10 box"]

testParseEval str = do
  let preP = parse rfvals "Test Parser" str
  let p = fromRight [RFPush (RFString "PARSE ERROR")] preP
  runInputT defaultSettings $ runEvalBaseCtx (evals p)
runTestExec = runInputT defaultSettings . runExecBaseCtx

main :: IO ()
main = hspec $ do
  parseTests
  evalTests

evalTests = do
  evalPushTests
  evalRefTests
  evalDefTests
  evalEvalTests
  evalEvalsTests
  describe "Fun tests?" $
    it "makes an 80x10 box" $
      testParseEval test1 `shouldReturn` ()

parseTests = do
  parseTokenTests
  parseDTypeTests
  parseRFValTests
  parseRFValsTests

parseTokenTests =
  describe "Parser token parsing" $ do
    it "handles an identifier" $
      parse identifier "Test Parser" "foo" `shouldBe` Right "foo"
    it "handles a positive integer" $
      parse integer "Test Parser" "3" `shouldBe` Right 3
    it "handles a negative integer" $
      parse integer "Test Parser" "-4" `shouldBe` Right (-4)
    it "handles a simple positive float" $
      parse float "Test Parser" "3.6" `shouldBe` Right 3.6
    it "handles a simple negative float" $
      parse float "Test Parser" "-3.6" `shouldBe` Right (-3.6)
    it "handles a positive E-notation float" $
      parse float "Test Parser" "3.6E3" `shouldBe` Right 3600
    it "handles a negative E-notation float" $
      parse float "Test Parser" "-3.6E3" `shouldBe` Right (-3600)
    it "handles a negative -E-notation float" $
      parse float "Test Parser" "-3.6E-3" `shouldBe` Right (-0.0036)
    it "handles a positive -E-notation float" $
      parse float "Test Parser" "3.6E-3" `shouldBe` Right 0.0036
    it "handles a bool" $
      parse bool "Test Parser" "true" `shouldBe` Right True

parseDTypeTests =
  describe "Parser primitive parsing" $ do
    it "handles an integer" $
      parse dtype "Test Parser" "4" `shouldBe` Right (RFInteger 4)
    it "handles a float" $
      parse dtype "Test Parser" "-3.6" `shouldBe` Right (RFFloat (-3.6))
    it "handles a bool" $
      parse dtype "Test Parser" "false" `shouldBe` Right (RFBool False)
    it "handles a quotation" $
      parse dtype "Test Parser" "(-3.6)" `shouldBe` Right (Quot [RFPush $ RFFloat (-3.6)])
    it "handles a string" $
      parse dtype "Test Parser" "\"Meep\"" `shouldBe` Right (RFString "Meep")

parseRFValTests =
  describe "Parser RFVal parsing" $ do
    it "handles a primitive" $
      parse rfval "Test Parser" "4" `shouldBe` Right (RFPush $ RFInteger 4)
    it "handles a definition" $
      parse rfval "Test Parser" ":foo 2;" `shouldBe` Right (Define "foo" (DefData [RFPush $ RFInteger 2]))
    it "handles a word reference" $
      parse rfval "Test Parser" "foo" `shouldBe` Right (Ref "foo")

parseRFValsTests =
  describe "Parser RFVals parsing" $ do
    it "handles multiple primitives" $
      parse rfvals "Test Parser" "foo 2 3.3 (1)"
        `shouldBe` Right [Ref "foo",RFPush $ RFInteger 2,RFPush $ RFFloat 3.3,RFPush $ Quot [RFPush $ RFInteger 1]]
    it "handles multiple definitions" $
      parse rfvals "Test Parser" ":foo 2; :bar false;"
        `shouldBe` Right [Define "foo" (DefData [RFPush $ RFInteger 2]),
          Define "bar" (DefData [RFPush $ RFBool False])]
    it "handles multiple word references" $
      parse rfvals "Test Parser" "foo bar baz"
        `shouldBe` Right [Ref "foo", Ref "bar", Ref "baz"]
    it "handles mixed prim/def/ref (1)" $
      parse rfvals "Test Parser" ":foo (4); foo bar"
        `shouldBe` Right [Define "foo" (DefData [RFPush $ Quot [RFPush $ RFInteger 4]]),
          Ref "foo", Ref "bar"]
    it "handles mixed prim/def/ref (2)" $
      parse rfvals "Test Parser" test0
        `shouldBe` Right [RFPush $ RFInteger 2,
          RFPush $ Quot [
            RFPush $ Quot [ RFPush $ Quot [RFPush $ RFInteger 3,Ref ">"],RFPush $ Quot[RFPush $ RFString "Greater than 3",Ref ".", Ref "cr"]]
              ,RFPush $ Quot [ RFPush $ Quot [RFPush $ RFInteger 3,Ref "<"], RFPush $ Quot[RFPush $ RFString "Smaller than 3",Ref ".", Ref "cr"]]
                ,RFPush $ Quot [ RFPush $ Quot [RFPush $ RFBool True], RFPush $ Quot [RFPush $ RFString "Exactly 3",Ref ".", Ref "cr"]]
                  ], Ref "case"]

evalPushTests =
  describe "evalPush" $ do
    it "handles simple Integer push" $
      runTestExec (evalPush (RFInteger 6)) `shouldReturn` (basicEnv {stack=[RFInteger 6]})
    it "handles simple Float push" $
      runTestExec (evalPush (RFFloat 3.6)) `shouldReturn` (basicEnv {stack=[RFFloat 3.6]})
    it "handles simple String push" $
      runTestExec (evalPush (RFString "Meep!")) `shouldReturn` (basicEnv {stack=[RFString "Meep!"]})
    it "handles simple Bool push" $
      runTestExec (evalPush (RFBool False)) `shouldReturn` (basicEnv {stack=[RFBool False]})
    it "handles simple quotation push" $
      runTestExec (evalPush (Quot [RFPush (RFInteger 4)]))
        `shouldReturn` (basicEnv {stack=[Quot [RFPush (RFInteger 4)]]})

evalRefTests =
  describe "evalRef" $ --Anything else to try here?
    it "handles push/pop ref test (look for '2' above)" $
      runTestExec (evalPush (RFInteger 2) >> evalRef ".")
        `shouldReturn` basicEnv

evalDefTests =
  describe "evalDef" $
    it "handles definition addition test" $
      runTestExec (evalDef "foo" (DefData [RFPush (RFInteger 3)]))
        `shouldReturn` (basicEnv {wordsMap = Map.fromList $
          ("foo",Right (DefData [RFPush (RFInteger 3)])) : primEnv})

evalEvalTests =
  describe "eval" $ do
    it "handles simple Float push" $
      runTestExec (eval (RFPush (RFFloat 3.6))) `shouldReturn` (basicEnv {stack=[RFFloat 3.6]})
    it "handles push/pop ref test (look for '2' above)" $
      runTestExec (eval (RFPush (RFInteger 2)) >> eval (Ref "."))
        `shouldReturn` basicEnv
    it "handles definition addition test" $
      runTestExec (eval (Define "foo" (DefData [RFPush (RFInteger 3)])))
        `shouldReturn` (basicEnv {wordsMap = Map.fromList $
          ("foo",Right (DefData [RFPush (RFInteger 3)])) : primEnv})


evalEvalsTests =
  describe "evals" $ do
    it "handles multiple pushes" $
      runTestExec (evals [RFPush (RFFloat 3.6), RFPush (RFBool False), RFPush (RFString "Meep :3")])
      `shouldReturn` (basicEnv {stack=[RFString "Meep :3", RFBool False, RFFloat 3.6]})
    it "handles a mix of pushes, refs, and defs" $
      runTestExec (evals [RFPush (RFFloat 3.6), RFPush (RFBool False),
        RFPush (RFString "Meep :3"),
        Define "foo" (DefData [RFPush (RFInteger 3)]),
        RFPush (Quot [RFPush (RFInteger 4)]), Ref "foo"])
      `shouldReturn` (basicEnv {
        stack=[RFInteger 3, Quot [RFPush (RFInteger 4)], RFString "Meep :3", RFBool False, RFFloat 3.6],
        wordsMap = Map.fromList $ ("foo",Right (DefData [RFPush (RFInteger 3)])) : primEnv })
    it "handles a simple foreach" $
      runTestExec (evals [RFPush $ Quot [RFPush (RFInteger 4), RFPush (RFInteger 8)], RFPush $ Quot [RFPush (RFInteger 2), Ref "*"], Ref "foreach"])
        `shouldReturn` (basicEnv {stack = [RFInteger 16, RFInteger 8]})
    it "stands the test of `times`" $
      runTestExec (evals [RFPush $ RFInteger 3,
        RFPush $ Quot [RFPush $ RFInteger 3, Ref "*", Ref "dup"],
        RFPush $ RFInteger 3, Ref "times", Ref "drop"])
        `shouldReturn` (basicEnv {stack=[RFInteger 81, RFInteger 27, RFInteger 9]})
    it "works even `while` looping (or being silly)" $
      runTestExec (evals [RFPush $ RFInteger 1, RFPush $ Quot [Ref "dup", RFPush $ RFInteger 10, Ref "<"],
        RFPush $ Quot [RFPush $ RFInteger 1, Ref "+", Ref "dup"], Ref "while"])
        `shouldReturn` (basicEnv {stack=[RFInteger 10, RFInteger 10, RFInteger 9, RFInteger 8, RFInteger 7,
          RFInteger 6, RFInteger 5, RFInteger 4, RFInteger 3, RFInteger 2 ]})
