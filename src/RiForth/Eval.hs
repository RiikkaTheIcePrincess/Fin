{-# LANGUAGE OverloadedStrings #-}
module RiForth.Eval where
import Control.Monad.State
import Control.Concurrent.MonadIO
import Control.Concurrent.STM.MonadIO
import Control.Concurrent.STM.TQueue
import qualified Data.Vector as V
import qualified Data.Map as Map
import Data.Either
import System.Console.Haskeline (InputT)
--Local
import RiForth.Parser
import RiForth.RFVal
import RiForth.Env
import RiForth.Prim
--DEBUG?
import Text.Megaparsec (parse)

runEval = flip (evalStateT . unEval)
basicEnv = EnvCtx (Map.fromList primEnv) V.empty V.empty []
runEvalBaseCtx = runEval basicEnv
runExec :: EnvCtx -> RFIFun s -> InputT IO EnvCtx
runExec = flip (execStateT . unEval)
runExecBaseCtx = runExec basicEnv
--DEBUG?
testEval str = do
  let preP = parse rfvals "Test Parser" str
  let p = fromRight [RFPush (RFString "PARSE ERROR")] preP
  runEvalBaseCtx (evals p >> testPrints)
testPrints :: RFIFun ()
testPrints = do
  e <- get
  liftIO $ print e
fromRight b (Left a) = b
fromRight _ (Right b) = b

evals :: [RFVal] -> RFIFun ()
evals (v:vs) = if null vs then eval v else eval v >> evals vs
evals [] = return ()
eval ::  RFVal -> RFIFun ()
eval v = case v of
  (RFPush e)      -> evalPush e
  (Ref w)         -> evalRef w
  (Define w def)  -> evalDef w def

evalPush :: DType -> RFIFun ()
evalPush v = do
  env <- get
  let ss = stack env
  put (env {stack = push v ss})
  return ()

evalRef :: TType -> RFIFun ()
evalRef w = do
  env <- get
  let wds = wordsMap env
  if not $ Map.member w wds then liftIO $ putStrLn ("Failed lookup for word " ++ w) else do
    let edef = wds Map.! w
    either
      id --Left (RFIFun ()) -> exec the RFIFun () from an internal definition
      (evals . getDefn) --Evaluate the Defn's code
      edef

evalDef :: TType -> Defn -> RFIFun ()
evalDef w d = do
  env <- get
  let nws = Map.insert w (Right d) (wordsMap env)
  put (env {wordsMap = nws})
  return ()
