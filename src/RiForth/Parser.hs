module RiForth.Parser where
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.Text as T
import Data.Void
import Control.Monad (void)
--Local
import RiForth.RFVal

type Parser = Parsec Void TType

takeUntil c = takeWhile1P Nothing (/= c)

sc :: Parser ()
sc = L.space space1 (L.skipLineComment "--") (L.skipBlockComment "{-" "-}")
signed :: Num a => Parser a -> Parser a
signed f = L.signed (return ()) f <?> "sign"
symbol :: TType -> Parser TType
symbol = L.symbol sc

identifier :: Parser TType
identifier = some idchars
  <?> "identifier"

idchars = digitChar <|> letterChar <|> oneOf "!$%&*/<=>?^_~+-.@" <?> "valid identifier character"

integer :: Parser Integer
integer = signed (try hexoctbin <|> L.decimal) <?> "integer"

hexoctbin :: Parser Integer
hexoctbin = single '0' *> (hexadecimal <|> octal <|> binary)
hexadecimal = char' 'x' *> L.hexadecimal
octal = char' 'o' *> L.octal
binary = char' 'b' *> L.binary

float :: Parser Double
float = signed L.float <?> "float"

bool :: Parser Bool
bool = True <$ (string "true" <|> try (string "#t"))
  <|> False <$ (string "false" <|> try (string "#f"))
  <?> "bool"

getDef :: Parser RFVal
getDef = do
  idr <- space *> identifier
  vs <- DefData <$> (space1 *> sepEndBy1 rfval' space1)
  return $ Define idr vs

rfvals :: Parser [RFVal]
rfvals = sepEndBy1 rfval space1 <* eof

rfval :: Parser RFVal
rfval =
  --Parse Define
  (between (single ':') (single ';') getDef <?> "start-of-definition")
  --Disallow nested definitions.
  <|> rfval'
  <?> "Top-level value (def/primitive/identifier)"

rfval' :: Parser RFVal
rfval' =
  try (RFPush <$> dtype)
  <|> Ref <$> identifier

dtype :: Parser DType
dtype =
  try (RFFloat <$> float <* notFollowedBy letterChar)
  <|> try (RFInteger <$> integer <* notFollowedBy (letterChar <|> oneOf "!$%&*/<=>?^_~+-.@"))
  <|> RFBool <$> bool
  <|> Quot <$> between (single '(' <* space) (single ')') (sepEndBy1 rfval space1)
  <|> RFString . T.pack <$> between (single '\"') (single '\"') (takeUntil '\"')
