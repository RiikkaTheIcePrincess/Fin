{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module RiForth.Env where
import Control.Monad.State
import Control.Concurrent.MonadIO
import Control.Concurrent.STM.MonadIO
import Control.Concurrent.STM.TBQueue
import qualified Data.Vector as V
import qualified Data.Map as Map
import Data.Either
import System.Console.Haskeline
--Local
import RiForth.RFVal

-- I like the clarity of Eval () especially since we use Eval (some type) sometimes
type RFIFun = Eval (InputT IO)
-- | Bidirectional TQueue
data BDTQueue a = BDTQ {inc :: TBQueue a, outc :: TBQueue a} deriving Eq

type Stack a = [] a
push :: a -> Stack a -> Stack a
push a = (a :)
pop :: Stack a -> (a,Stack a)
pop s = (head s, tail s)

-- The whole VM state, really. Maybe there's a preferable way?
data EnvCtx = EnvCtx  { wordsMap :: Map.Map String (Either (Eval (InputT IO) ()) Defn)
                      , ioPorts :: V.Vector (BDTQueue DType)
                      , memory :: V.Vector DType
                      , stack :: Stack DType
                      }
instance Show EnvCtx where
  show (EnvCtx wm ioP mem ss) = "S: " ++ show ss ++ "\nWords: " ++ show (Map.keys wm) ++ "\nmem: " ++ show mem
instance Eq EnvCtx where
  (EnvCtx wm1 iop1 mem1 s1) == (EnvCtx wm2 iop2 mem2 s2) =
    Map.keys wm1 == Map.keys wm2 && iop1 == iop2 && mem1 == mem2 && s1 == s2

newtype Monad m => Eval m a = Eval { unEval :: StateT EnvCtx m a }
  deriving (Monad
           , Functor
           , Applicative
           , MonadState EnvCtx
           , MonadIO
           , MonadTrans)
