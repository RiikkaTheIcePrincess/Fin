{-# LANGUAGE ScopedTypeVariables, RankNTypes #-}
module RiForth.Prim (primEnv) where
--import System.IO
import Control.Monad
import Control.Monad.State
import Control.Concurrent (threadDelay) -- threadDelay µs
import Control.Concurrent.STM.MonadIO (atomically, STM)
import Control.Concurrent.STM.TBQueue
import qualified Data.Map.Lazy as Map
import qualified Data.Vector as V
import System.Console.Haskeline
--Local
import RiForth.RFVal
import RiForth.Env
import {-# SOURCE #-} RiForth.Eval
{-# ANN module "HLint: ignore Reduce duplication" #-}

-- | Function to handle background processing (like trimming IO buffers)
-- |Also serves to simulate slow memory access
doBGSleep :: RFIFun ()
doBGSleep = do
  sleep sleepDuration
  ios <- gets ioPorts
  mapM_ ringTrim ios

ringTrim :: BDTQueue a -> RFIFun ()
ringTrim q = atomically $ do
  f <- isFullTBQueue $ inc q
  when f (void $ readTBQueue (inc q))

--FIXME TODO ETC
-- Set properly through VM config
sleepDuration = 13
--Possible words to add:
-- nip ( x1 x2 -- x2 ) (Drop the item just under the top of the stack)
-- ask (user input)
-- ? (show value at given address)

-- Built-in words
primEnv :: [(TType,Either (RFIFun ()) Defn)]
primEnv = [
    ("+"       , Left add  )
  , ("*"       , Left mult )
  , ("/"       , Left divi )
  , ("mod"     , Left modF )
  , ("-"       , Left subt )
  , ("<"       , Left less )
  , ("<="      , Left leq  )
  , (">"       , Left grt  )
  , (">="      , Left geq  )
  , ("=="      , Left eq   )
  , ("and"     , Left also )
  , ("or"      , Left orF  )
  , ("not"     , Left notF )
  , ("."       , Left popSpace)
  , ("print"   , Left popShow)
  , ("puts"    , Left puts)
  , ("allot"   , Left malloc)
  , ("!"       , Left memput)
  , ("@"       , Left memget)
  , ("depth"   , Left depth)
  , ("dup"     , Left dup)
  , ("if"      , Left ifF)
  , ("while"   , Left whileF)
  , ("swap"    , Left swap)
  , ("case"    , Left caseF)
  , ("when"    , Left whenF)
  , ("times"   , Left times)
  , ("foreach" , Left foreach)
  , ("drop"    , Left dropF) --Forth's `DROP`, not min's `drop`
  , ("quote"   , Left quote)
  , ("dequote" , Left dequote)
  , ("words"   , Left wordsF)
  , ("do"      , Left doF)
  , ("cr"      , Left cr)
  , ("send"    , Left send)
  , ("recv"    , Left recv)
  , ("over"    , Left over)
  , ("2dup"    , Left twodup)
  , (".S"      , Left printStack)
  , ("i"       , Right (DefData [RFPush (RFInteger 0)]))
  ]

-- |Unary boolean operation using a stack argument
unSBOp :: (Bool -> Bool) -> RFIFun ()
unSBOp oper = withBool' (\v -> epush (RFBool $ oper v))

-- |Binary operation using stack arguments
binSOp :: (DType -> DType -> DType) -> RFIFun ()
binSOp oper = do
  v1 <- epop
  v2 <- epop
  epush (oper v2 v1)

-- |Binary arithmetic operation using the stack
binSAOp :: (forall a. Num a => a -> a -> a) -> DType -> DType -> DType
binSAOp oper v1 v2 = binSAOp' oper (convertRFAs v1 v2)
binSAOp' :: (forall a. Num a => a -> a -> a) -> Either (Integer,Integer) (Double,Double) -> DType
binSAOp' oper = either (RFInteger . f) (RFFloat . f)
  where
    f (v1,v2) = oper v1 v2

-- |Binary relational operation on numeric arguments
binSAROp :: (forall a. Ord a => a -> a -> Bool) -> DType -> DType -> DType
binSAROp oper v1 v2 = binSAROp' oper (convertRFAs v1 v2)
binSAROp' :: (forall a. Ord a => a -> a -> Bool) -> Either (Integer,Integer) (Double,Double) -> DType
binSAROp' oper e = RFBool $ either f f e --Ew?
  where
    f (v1,v2) = oper v1 v2

binSBOp :: (Bool -> Bool -> Bool) -> DType -> DType -> DType
binSBOp oper (RFBool v1) (RFBool v2) = RFBool (oper v1 v2)

-- |Convert RFInteger pair to Integer or any pair with RFFloat to Double. Better way? :(
convertRFAs :: DType -> DType -> Either (Integer,Integer) (Double,Double)
convertRFAs (RFInteger v1) (RFInteger v2) = Left (v1,v2)
convertRFAs (RFFloat v1) (RFInteger v2) = Right (v1,fromIntegral v2)
convertRFAs (RFInteger v1) (RFFloat v2) = Right (fromIntegral v1,v2)
convertRFAs (RFFloat v1) (RFFloat v2) = Right (v1,v2)

add  = binSOp (binSAOp (+))
divi = binSOp divi'
mult = binSOp (binSAOp (*))
subt = binSOp (binSAOp (-))
less = binSOp (binSAROp (<))
leq  = binSOp (binSAROp (<=))
grt  = binSOp (binSAROp (>))
geq  = binSOp (binSAROp (>=))
eq   = binSOp (binSAROp (==))
also = binSOp (binSBOp (&&)) --TODO Wait, this is supposed to be a bitwise AND?? Weird.
orF  = binSOp (binSBOp (||)) --TODO Check this too
notF = unSBOp not

-- |print ( x1 -- ) Prints the top of the stack
popShow :: RFIFun ()
popShow = epop >>= (out . writeDType)
-- |. ( x1 -- ) Prints the top of the stack with a space after
popSpace :: RFIFun ()
popSpace = popShow >> out " "
-- |puts ( x1 -- ) Prints the top of the stack with a newline after
puts :: RFIFun ()
puts = popShow >> cr

-- |/ ( x1 x2 -- x3 ) Places x1/x2 atop the stack, using integer division
-- |iff both operands are integer type, otherwise floating-point
divi' :: DType -> DType -> DType
divi' v1 v2 = either (RFInteger . f div) (RFFloat . f (/)) conv
  where
    conv = convertRFAs v1 v2
    f op (v1',v2') = op v1' v2'

modF :: RFIFun ()
modF =
  withInt' $ \v2 ->
    withInt' $ \v1 ->
      epush (RFInteger $ v1 `mod` v2)

-- |allot ( x1 -- addr1 ) Places atop the stack the address of a new region of
-- |x1 (integer) values of allocated memory space
malloc :: RFIFun ()
malloc = do
  m <- gets memory
  let mlen = V.length m
  withInt' $ \v -> do
    --Push beginning address of allocated area
    epush (RFInteger $ fromIntegral mlen)
    let m' = m V.++ V.replicate (fromInteger v) (RFInteger 0)
    env <- get
    put (env{memory = m'})

-- |! ( x1 addr1 -- ) places the top of the stack into memory at the given address
memput :: RFIFun ()
memput =
  withInt' $ \addr -> do
    env <- get
    let m = memory env
    let s = stack env
    epop >>= mempoke (fromInteger addr)

-- |@ ( addr1 -- x1 ) places the contents of the given memory address atop the stack
memget :: RFIFun ()
memget = do
  env <- get
  let m = memory env
  let s = stack env
  withInt' $ \ad -> mempeek (fromInteger ad) >>= epush

-- |depth ( -- x1 ) places the current stack depth atop the stack
depth :: RFIFun ()
depth = do
  s <- gets stack
  epush (RFInteger (fromIntegral $ length s))

-- |dup ( x1 -- x1 x1 ) duplicates the item at the top of the stack
dup :: RFIFun ()
dup = do
  v <- epop
  epush v
  epush v

-- |if ( (condition) (action1) (action2) -- * )
-- |Executes action1 if condition evaluates to true, otherwise executes action2
ifF :: RFIFun ()
ifF =
  withQuot' $ \pl2 ->
    withQuot $ \pl1 -> do
      let f p = if p then evals pl1 else evals pl2
      pred' <- checkPred
      maybe (return ()) f pred'

-- |when ( (condition) (action) -- * )
-- |Executes action if condition evaluates to true
whenF :: RFIFun ()
whenF =
  withQuot' $ \pl -> do
    let f p = when p $ evals pl
    pred' <- checkPred
    maybe (return ()) f pred'

-- |while ( (condition) (action) -- * )
-- |Executes action repeatedly as long as condition evaluates to true
whileF :: RFIFun ()
whileF =
  withQuot' $ \pl ->
    withQuot' $ \cond -> do
      setLoopVar 0
      whileF' cond pl 0
whileF' :: [RFVal] -> [RFVal] -> Integer -> RFIFun ()
whileF' cond pl i = do
  evals cond --Should leave a boolean result atop the stack
  withBool' $ \pred' -> do
    setLoopVar i
    when pred' $ evals pl >> whileF' cond pl (i+1)

-- |swap ( x1 x2 -- x2 x1 ) Swaps the order of the top two items on the stack
swap :: RFIFun ()
swap = do
  v1 <- epop
  v2 <- epop
  epush v1
  epush v2

-- |case ( ((condition) (action))* -- *)
-- |Evaluates the condition of ((condition) (action)) pairs in order until a
-- |condition evaluates to true, then executes the associated action.
caseF :: RFIFun ()
caseF =
  withQuot' $ \preQuots -> do
    preSize <- stackLen --S size before getting cases
    evals preQuots
    newSize <- stackLen --S size after getting cases
    --Number of cases (each one is in a quotation on the S stack)
    let numCases = newSize - preSize
    cases <- reverse <$> forM [1..numCases] (const epop)
    caseF' cases
caseF' :: [DType] -> RFIFun ()
caseF' (Quot c:cs) = do -- S: ((pred) (payload))
  evals c -- S: (payload) (pred)
  withQuot' $ \pl ->
    withQuot' $ \pred' -> do -- S empty
      --Store the environment from before we possibly consume stack items testing pred
      env <- get
      evals pred' -- S: (pred result)
      withBool' $ \pred'' ->
        if pred'' then evals pl
          else do
            put env
            caseF' cs
caseF' [] = return ()

-- |times ( (action) n -- * ) Executes action n times
times :: RFIFun ()
times =
  withInt' $ \n ->
    withQuot' $ \q -> do
      setLoopVar 0
      forM_ [1..n] (\i -> setLoopVar i >> evals q)

-- |foreach ( (xs) (action) -- ) Executes action upon each item in xs
foreach :: RFIFun ()
foreach =
  withQuot' $ \op ->
    withQuot' $ \vs -> do
      setLoopVar 0
      --forM_ vs (\v -> updateLoopVar >> eval v >> evals op)
      forM_ [0..length vs - 1] (\i -> setLoopVar (toInteger i) >> eval (vs !! i) >> evals op)

-- |drop ( x1 -- ) Discards the item at the top of the stack
dropF :: RFIFun ()
dropF = void epop

-- |quote ( x1 -- (x1) ) Encases the value atop the stack within a quotation
quote :: RFIFun ()
quote = do
  v <- epop
  epush $ Quot [RFPush v]

-- |dequote ( (action) -- * ) Executes the quotation atop the stack
dequote :: RFIFun ()
dequote =
  withQuot' $ \vs ->
    evals vs

-- |words ( -- ) Prints a list of currently available words
wordsF :: RFIFun ()
wordsF = do
  wm <- gets wordsMap
  epush (RFInteger $ fromIntegral $ length (Map.keys wm))

-- |cr ( -- ) Prints a newline
cr :: RFIFun ()
cr = outLn ""

-- |do ( limit start (action) -- * )
-- |Executes action for each number between start and limit inclusive
doF :: RFIFun ()
doF =
    withQuot' $ \pl ->
      withInt' $ \start ->
        withInt' $ \limit -> do
          setLoopVar start
          forM_ [start..limit] (\i -> setLoopVar i >> evals pl)

-- |send ( x1 channelID -- ) Sends x1 along the given channel ID
-- |Semantics of IO sim: only one item is going over the wire at a time
send :: RFIFun ()
send =
  withInt' $ \chan -> do
    let chanID = fromIntegral chan :: Int
    v <- epop
    ios <- gets ioPorts
    if chanID >= V.length ios || chanID < 0 then
      outLn "NOT OK: Attempted access to a channel outside of this device's supported range"
      else atomically $ writeTBQueue (outc $ ios V.! chanID) v

-- |recv ( channelID -- x1 ) Attempts to receive a value from the given channel ID
-- |Returns (upon the stack) an item or blocks until it can
recv :: RFIFun ()
recv =
  withInt' $ \chan -> do
    let chanID = fromIntegral chan :: Int
    ios <- gets ioPorts
    if chanID >= V.length ios || chanID < 0 then
      outLn "NOT OK: Attempted access to a channel outside of this device's supported range"
      else atomically (readTBQueue (inc $ ios V.! chanID)) >>= epush

-- |over ( x1 x2 -- x1 x2 x1 )
-- |Pulls a copy of the value under the top of the stack to the top of the stack
over :: RFIFun ()
over = do --S: x2 x1
  v2 <- epop --S: x1
  v1 <- epop --S: empty
  epush v1 --S: x1
  epush v2 --S: x2 x1
  epush v1 --S: x1 x2 x1

-- |2dup ( x1 x2 -- x1 x2 x1 x2 ) Duplicates the top two items of the stack,
-- |in order
twodup :: RFIFun ()
twodup = over >> over

-- |.S ( xs -- xs ) Prints the current contents of the stack
printStack :: RFIFun ()
printStack = do
  s <- gets stack
  outLn (concatMap ((++ " ") . writeDType) s)

--Convenience/support functions
setLoopVar :: Integer -> RFIFun ()
setLoopVar ix = updateWord "i" (const (Just $ Right (DefData [RFPush (RFInteger ix)])))

-- The prettiest function
updateWord :: TType ->
  (Maybe (Either (Eval (InputT IO) ()) Defn) ->
  Maybe (Either (Eval (InputT IO) ()) Defn)) ->
  RFIFun ()
updateWord w f = do
  env <- get
  let ws = wordsMap env
  let ws' = Map.alter f w ws
  put (env {wordsMap = ws'})

isInt :: DType -> (Bool,TType)
isInt (RFInteger x) = (True,"")
isInt _ = (False,"NOT OK: Expected Integer atop the stack")
isBool :: DType -> (Bool,TType)
isBool (RFBool x) = (True,"")
isBool _ = (False,"NOT OK: Expected Bool atop the stack")
isFloat :: DType -> (Bool,TType)
isFloat (RFFloat x) = (True,"")
isFloat _ = (False,"NOT OK: Expected Float atop the stack")
isQuot :: DType -> (Bool,TType)
isQuot (Quot x) = (True,"")
isQuot _ = (False,"NOT OK: Expected Quotation atop the stack")

popBool :: RFIFun Bool
popBool = do
  RFBool b <- epop
  return b
popFloat :: RFIFun Double
popFloat = do
  RFFloat f <- epop
  return f
popInt :: RFIFun Integer
popInt = do
  RFInteger i <- epop
  return i
popQuot :: RFIFun [RFVal]
popQuot = do
  Quot q <- epop
  return q

withQuot :: ([RFVal] -> RFIFun a) -> RFIFun (Maybe a)
withQuot act = checkOrAbort isQuot (popQuot >>= act)
-- |with' functions void the resulting Maybe
-- |Computation has been aborted by then
-- |Non-'returning' functions have no need to do anything after this
withQuot' = void . withQuot
withBool :: (Bool -> RFIFun a) -> RFIFun (Maybe a)
withBool act = checkOrAbort isBool (popBool >>= act)
withBool' = void . withBool
withFloat :: (Double -> RFIFun a) -> RFIFun (Maybe a)
withFloat act = checkOrAbort isFloat (popFloat >>= act)
withFloat' = void . withFloat
withInt :: (Integer -> RFIFun a) -> RFIFun (Maybe a)
withInt act = checkOrAbort isInt (popInt >>= act)
withInt' = void . withInt

-- | TODO better name?
-- | checks type atop the stack and continues actions if it matches
-- | Else outputs given error message and stops
checkOrAbort :: (DType -> (Bool,TType)) -> RFIFun s -> RFIFun (Maybe s)
checkOrAbort test act = do
  (ver,errMsg) <- checkPop test
  if ver then Just <$> act
    else outLn errMsg >> return Nothing

checkPop :: (DType -> (Bool,TType)) -> RFIFun (Bool,TType)
checkPop test = do
  s <- gets stack
  if null s then
    return (False,"NOT OK: Stack underflow")
    else return $ test (head s)

checkPred :: RFIFun (Maybe Bool)
checkPred = join <$> checkPred'
checkPred' =
  withQuot $ \cond -> do
    evals cond --Should leave a boolean result atop the stack
    withBool return

outLn :: String -> RFIFun ()
outLn = lift . outputStrLn
out :: String -> RFIFun ()
out = lift . outputStr

stackLen :: RFIFun Int
stackLen = length <$> gets stack

epop :: RFIFun DType
epop = do
  env <- get
  let s = stack env
  if Prelude.null s then outLn "NOT OK: Stack underflow" >> return Bottom else do
    let (v,s') = pop s
    put (env{stack = s'})
    doBGSleep
    return v

epush :: DType -> RFIFun ()
epush v = do
  env <- get
  let s = stack env
  let s' = push v s
  put (env{stack = s'})
  doBGSleep

-- | Sleep current thread for a number of milliseconds
sleep :: Int -> RFIFun ()
sleep = liftIO . threadDelay . (1000*)

mempeek :: Int -> RFIFun DType
mempeek addr = do
  doBGSleep
  m <- gets memory
  if addr >= V.length m then outLn "NOT OK: Access beyond memory bounds" >> return Bottom
    else return (m V.! addr)

mempoke :: Int -> DType -> RFIFun ()
mempoke addr v = do
  doBGSleep
  env <- get
  let m = memory env
  if addr >= V.length m then outLn "NOT OK: Access beyond memory bounds"
    else do
      let m' = m V.// [(addr,v)]
      put (env{memory = m'})
