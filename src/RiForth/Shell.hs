{-# LANGUAGE OverloadedStrings, TypeOperators #-}
module RiForth.Shell where
import System.Console.Haskeline
import Data.List (elemIndices)
import qualified Data.Vector as V
import Control.Concurrent.MonadIO (liftIO)
import Control.Concurrent.STM.MonadIO (atomically)
import Control.Concurrent.STM.TBQueue
import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (replicateM)
import Text.Megaparsec (parse, errorBundlePretty)
import Numeric.Natural
--Local
import RiForth.RFVal
import RiForth.Eval
import RiForth.Env
import RiForth.Parser

-- | Make n channels each with m values of buffer space
mkChans :: Int -> Natural -> IO (V.Vector (BDTQueue DType))
mkChans n m = V.fromList <$>
  replicateM n (BDTQ <$>
    (newTBQueueIO m :: IO (TBQueue DType)) <*>
      newTBQueueIO m)

-- | Entry point: the path of the RFShell!
-- Channel number/buffer values are placeholders
path :: V.Vector (BDTQueue DType) -> IO ()
path cs = 
  --cs <- mkChans 8 8
  --forkIO $ testPeriph (cs V.! 0) --TESTING DEBUG FIXME TODO CUPCAKES
  runInputT defaultSettings (intro >> gTree (basicEnv { ioPorts = cs}))
  --return cs

--TESTING DEBUG FIXME TODO CUPCAKES
testPeriph c = do
  atomically $ writeTBQueue (inc c) (RFInteger 3)
  threadDelay 3000000 -- 3s * 1000 ms/s * 1000 µs/ms
  testPeriph c

intro :: InputT IO ()
intro = do
  outputStrLn "Welcome to the RiForth shell! \\ö/"
  outputStrLn "To exit, say \":q\" on an empty line."
  outputStrLn "To cancel multi-line input, say \":c\" on an empty line.\n\n"

gTree :: EnvCtx -> InputT IO ()
gTree env = do
  line <- getInputLine "RFShell> "
  --[Joke comment] This is a case expression
  case line of
    Nothing -> outputStrLn "ö/" --Wave goodbye and quit
    Just ":q" -> outputStrLn "ö/" --Wave goodbye and quit
    Just "" -> gTree env
    Just "\n" -> gTree env
    Just "\r\n" -> gTree env
    --[Silly comment] Don't wave or quit but parse and evaluate input as appropriate
    Just input -> do
      input' <- checkGetInput input
      parsed <- getParsed input'
      runExec env (evals parsed) <* outputStrLn "" >>= gTree

checkGetInput :: String -> InputT IO String
checkGetInput str =
  if checkDefs str && checkQuots str then return str
    else getInputLine "+> " >>= \nsM ->
      case nsM of
        Nothing -> outputStrLn "Input cancelled, expect errors" >> return str
        Just ":c" -> outputStrLn "Input cancelled, expect errors" >> return str
        Just ns -> checkGetInput (str ++ '\n' : ns)

checkDefs :: String -> Bool
checkDefs str = length (elemIndices ':' str) == length (elemIndices ';' str)
checkQuots :: String -> Bool
checkQuots str = length (elemIndices '(' str) == length (elemIndices ')' str)

getParsed :: TType -> InputT IO [RFVal]
getParsed str = do
  let preP = parse rfvals "RFShell" str
  either (\err -> (outputStrLn . errorBundlePretty) err >> return []) return preP
