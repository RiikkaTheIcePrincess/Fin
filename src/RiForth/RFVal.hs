module RiForth.RFVal where
import qualified Data.Text as T

--General text type for internals. RFStrings use Text but
--[Char] are convenient elsewhere
type TType = String
{-|
  Parseable bits. We need to parse for:

  * Definitions have a special structure: ": id defn ;" where id is the word
  and defn is a sequence of non-definition RFVals
  * "Ref" is just any word invocation
  * RFPush refers to pushing onto the stack, the main thing we do in this language.
-}
data RFVal =
  -- |Push a plain DType to the stack
  RFPush DType
  -- |Add a definition to the definition memory
  | Define String Defn
  -- |Reference to a stored (word) definition
  | Ref String
  deriving (Show, Eq)

-- |List of values stored in definition memory
newtype Defn = DefData [RFVal]
  deriving (Show, Eq)

getDefn (DefData vs) = vs

-- |Low-level data types
data DType =
  RFString T.Text
  | RFFloat Double
  | RFInteger Integer
  | RFBool Bool
  -- |min-style quotation
  | Quot [RFVal]
  | Bottom --Failure type/represents lack of any valid type
  deriving Eq

instance Show DType where
  show (RFString tx) = "\"" ++ T.unpack tx ++ "\""
  show (RFFloat v) = show v
  show (RFInteger i) = show i
  show (RFBool b) = show b
  show (Quot vs) = "(" ++ show vs ++ ")"
  show Bottom = "💥\n"

-- |For shell output
writeDType :: DType -> String
writeDType (RFString v) = T.unpack v
writeDType (RFFloat f) = show f
writeDType (RFInteger i) = show i
writeDType (RFBool b) = show b
writeDType (Quot vs) = "(Quotation)" --"(" ++ (concat $ map writeDType vs) ++ ")"
writeDType Bottom = "💥\n"
