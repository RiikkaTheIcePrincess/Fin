import qualified Data.Vector as V
import Control.Concurrent (forkIO)
--
import RiForth
--import DisplayPeriph

main = do
  --cs <- path
  cs <- mkChans 8 8
  --forkIO $ simpleDisplay (outc (cs V.! 0))
  path cs
